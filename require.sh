#!/usr/bin/env bash

## NOTE that we assume without checking that 'logging' is available to us.
## We do this because both this file and the 'logging' file are sourced
## (in order) by 'import.sh', which we would otherwise use to import logging.
## because both are sourced by 'import.sh', both should always be available to any
## user of 'import.sh', and 'logging' should always be available here in any context
## in which 'import.sh' is being used.
set -eu

function require_var {
  local var_name="$1"
  local messages=${*:2}
  if [[ -z "${!var_name}" ]]; then
    log_error $messages
    return 1
  fi
}

function ensure_var {
  local var_name="$1"
  local default="$2"
  local messages=${*:3}
  if require default \
             "no default value specified for var $var_name!"
  then
    if [[ -n "${!var_name}" ]]; then
      echo "${!var_name}"
    else
      log_info $messages
      echo "$default"
    fi
  else
    return 1
  fi
}

function require_command {
  local target="$1"
  local res=1
  if [[ -z "$target" ]]; then
    log_error "called with no arguments!"
  elif command -v "$target" > /dev/null; then
    local res=0
  else
    log_error "'$target' executable is required, but we couldn't find it!" \
              "Please install $target and try again!"
  fi
  return "$res"
}
