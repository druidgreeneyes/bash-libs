#!/usr/bin/env bash

_trace=" 0 t trace "
_debug="${_trace}1 d debug "
_info="${_debug}2 i info "
_warn="${_info}3 w warn "
_error="${_warn}4 e error "


export LOGGING_CONTEXT_TRUNCATE_LENGTH=${LOGGING_CONTEXT_TRUNCATE_LENGTH:-50}
export LOG_CONTEXT="${LOG_CONTEXT:-}"
export LOG_PREFIX="${LOG_PREFIX:-}"
export LOG_LEVEL="${LOG_LEVEL:-info}"

function log
{
  if [[ -n "$LOG_CONTEXT" ]]; then
    local LOG_CONTEXT="$LOG_CONTEXT "
  fi
  if [[ -n "$LOG_PREFIX" ]]; then
    local LOG_PREFIX="[$LOG_PREFIX] "
  fi
  for line in $*; do
    >&2 echo "${LOG_PREFIX}${LOG_CONTEXT}$line"
  done
}

function log_level
{
  [[ -n $LOG_LEVEL && "$1" == *" $LOG_LEVEL "* ]]
}

function log_trace
{
  local IFS=$'\n'
  if log_level "$_trace"; then
    local LOG_PREFIX="TRACE"
    log $*
  fi
}

function log_debug
{
  local IFS=$'\n'
  if log_level "$_debug"; then
    local LOG_PREFIX="DEBUG"
    log $*
  fi
}

function log_info
{
  local IFS=$'\n'
  if log_level "$_info"; then
    local LOG_PREFIX="INFO"
    log $*
  fi
}

function log_warn
{
  local IFS=$'\n'
  if log_level "$_warn"; then
    local LOG_PREFIX="WARN"
    log $*
  fi
}

function log_error
{
  local IFS=$'\n'
  if log_level "$_error"; then
    local LOG_PREFIX="ERROR"
    log $*
  fi
}

function _logging_compress_context_slug
{
  local delim=$1
  local IFS=$delim
  local flip=1
  for input in $2; do
    if [[ $flip -eq 1 && ${#input} -gt 1 ]]; then
      printf "${input:0:1}"
      flip=0
    else
      printf "$input"
    fi
    printf "$delim"
  done
  printf "\n"
}

function add_log_context
{
  ctx=$1
  local LOG_CONTEXT="$LOG_CONTEXT$ctx:"
  while [[ ${#LOG_CONTEXT} -gt $LOGGING_CONTEXT_TRUNCATE_LENGTH ]]; do
    local LOG_CONTEXT=$(_logging_compress_context_slug ":" $LOG_CONTEXT)
  done
  echo $LOG_CONTEXT
}
